# VRG vstupní demo

Za využití frameworku React a knihovny OpenLayers vytvořte jednoduchou webovou aplikaci, která bude umět:

- Zobrazit mapu z online zdroje
- Poskytovat následující nástroje:
  - *Měření dálky* - zobrazení délky a azimutu zadané úsečky v mapě
  - *Měření úhlu* - zobrazení velikosti úhlu svíraného dvěma úsečkami s jedním společným bodem
  - *Kreslení a modifikace nakreslené polyčáry*

Parametry potřebné pro uvedenou funkcionalitu je možné zadat pomocí myši v mapě anebo číselně pomocí vstupních ovládacích prvků. Tzn. je nutné vytvořit a vhodně použít input controly pro zadání, zobrazení a editaci úhlu a vzdálenosti číselnými hodnotami s tím, že bude možné přepínat jednotky (např. kilometry/míle, stupně/radiány).

Vizuální stránka, způsob a míra komplexity zvoleného řešení je na volbě řešitele. Vypracovanou aplikaci je vhodné opatřit návodem na spuštění (to by mělo být co nejjednodušší).